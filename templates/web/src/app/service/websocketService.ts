/**
 * Created by enixjin on 6/13/18.
 */
import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import * as io from 'socket.io-client';
import {DOMAIN_SERVER} from "../constants";

@Injectable({
    providedIn: 'root',
})
export class WebsocketService {

    private socket;

    public deviceData$: Subject<any> = new Subject();

    constructor() {
        this.socket = io(DOMAIN_SERVER);

        this.socket.on("device data", (data: any) => {
            this.deviceData$.next(data);
        })
    }

}
