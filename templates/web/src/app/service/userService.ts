/**
 * Created by enixjin on 1/3/17.
 */
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {catchError} from "rxjs/operators";
import {DOMAIN_SERVER_URL, USER_URL} from "../constants";
import {User} from "../module/User";
import {BaseService} from "./baseService";

@Injectable({
    providedIn: 'root',
})
export class UserService extends BaseService<User> {

    constructor(http: HttpClient) {
        super(http);
    }

    getServiceUrl(): string {
        return DOMAIN_SERVER_URL + USER_URL;
    }


    login(username: string, password: string): Observable<any> {
        return this.http
            .post(this.getServiceUrl() + "/login", {
                username: username,
                password: password
            })
            .pipe(
                catchError(this.handleError)
            );
    }

}
