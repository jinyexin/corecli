/**
 * Created by enixjin on 4/9/18.
 */

export class ServiceError {
    constructor(public message: string, public statusCode: number, public error: string) {
    }
}
