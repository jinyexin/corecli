/**
 * Created by enixjin on 1/3/17.
 */
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";
import * as constants from "../constants";
import {BaseModule} from "../module/BaseModule";
import {PageInfo} from "../module/pagination/PageInfo";
import {ServiceError} from "./error/ServiceError";

export abstract class BaseService<T extends BaseModule> {
    protected _serviceServerBaseUrl: string = constants.DOMAIN_SERVER_URL;

    public abstract getServiceUrl(): string;

    constructor(protected http: HttpClient) {
    }

    protected handleError(errorResponse: HttpErrorResponse): Observable<any> {
        if (errorResponse.status === 0) {
            return throwError(new ServiceError(errorResponse.statusText, 0, errorResponse.message));
        } else {
            return throwError(new ServiceError(errorResponse.error.message, errorResponse.status, errorResponse.error.error));
        }
    }

    protected getJsonHeader(params?: HttpParams) {
        let headers = new HttpHeaders();
        headers = headers.append("Content-Type", "application/json");
        headers = headers.append("jwt", localStorage.getItem("token"));
        if (params) {
            return {headers, params};
        } else {
            return {headers};
        }
    }

    protected getLoginUser() {
        return JSON.parse(localStorage.getItem("loginUser"));
    }

    protected getJsonHeaderWithoutJWT(params?: HttpParams) {
        let headers = new HttpHeaders();
        headers = headers.append("Content-Type", "application/json");
        if (params) {
            return {headers, params};
        } else {
            return {headers};
        }
    }

    delete(id: number) {
        return this.http.delete(this.getServiceUrl() + "/" + id, this.getJsonHeader())
            .pipe(
                catchError(this.handleError)
            );
    }

    create(domainObject: T) {
        return this.http.post(this.getServiceUrl(), JSON.stringify(domainObject), this.getJsonHeader())
            .pipe(
                catchError(this.handleError)
            );
    }

    update(domainObject: T) {
        return this.http.put(this.getServiceUrl() + "/" + domainObject.id, JSON.stringify(domainObject), this.getJsonHeader())
            .pipe(
                catchError(this.handleError)
            );
    }

    batchUpdate(domainObject: T[]) {
        return this.http.put(this.getServiceUrl(), JSON.stringify(domainObject), this.getJsonHeader())
            .pipe(
                catchError(this.handleError)
            );
    }

    get(id: number) {
        return this.http.get<T>(this.getServiceUrl() + "/" + id, this.getJsonHeader())
            .pipe(
                catchError(this.handleError)
            );
    }

    list() {
        return this.http.get<T[]>(this.getServiceUrl(), this.getJsonHeader())
            .pipe(
                catchError(this.handleError)
            );
    }

    search(params: HttpParams) {
        return this.http.get<T[]>(this.getServiceUrl(), this.getJsonHeader(params))
            .pipe(
                catchError(this.handleError)
            );
    }

    pagination(pageInfo: PageInfo) {
        return this.http.post(this.getServiceUrl() + "/pagination", JSON.stringify(pageInfo), this.getJsonHeader())
            .pipe(
                catchError(this.handleError)
            );
    }
}
