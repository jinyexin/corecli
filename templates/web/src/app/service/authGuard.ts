/**
 * Created by enixjin on 12/22/17.
 */

import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from "@angular/router";

@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanActivateChild {
    backURL: string;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const url: string = state.url;
        return this.checkLogin(url);
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }

    checkLogin(url: string): boolean {
        const loginUser = JSON.parse(localStorage.getItem("loginUser"));
        const token = localStorage.getItem("token");
        if (!loginUser || !token) {
            this.backURL = url;
            this.router.navigate(["/login"]);
            return false;
        } else {
            return true;
        }
    }

}
