import {BaseModule} from "./BaseModule";

export class User extends BaseModule {
    id: number;
    username: string;
    password: string;
}

