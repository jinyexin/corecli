/**
 * Created by enixjin on 3/28/18.
 */

export class PageInfo {
    pageSize = 10;
    count = 0; // search result total
    total = 0;
    offset = 0; // pageNumber
    searchAllColumn: string = "";
    sorts?: sort[];
    columns?: Array<columnSearch>;
}

export class columnSearch {
    name: string;
    value?: any;
    join?: "or" | "and";
    matchExactly?: boolean;
}

export class sort {
    dir: "asc" | "desc";
    column: string;
}
