import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {LoadingModule} from "ngx-loading";
import {ToastrModule} from 'ngx-toastr';
import {AppComponent} from './app.component';
// Import routing module
import {AppRoutingModule} from './app.routing';
import {LoginComponent} from "./components/app-login/login.component";
import {LogoutComponent} from "./components/app-logout/logout.component";
import {RegisterComponent} from "./components/app-register/register.component";
// Import containers
import {FullLayoutComponent} from './containers/full-layout';
import {AuthGuard} from "./service/authGuard";
import {MessageService} from "./service/messageService";
import {UserService} from "./service/userService";
import {SharedModules} from "./shared/sharedModules";

// Import 3rd party components

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        SharedModules,
        LoadingModule,
        ToastrModule.forRoot({
            timeOut: 5000,
            positionClass: 'toast-bottom-right',
        })
    ],
    declarations: [
        AppComponent,
        FullLayoutComponent,
        LoginComponent,
        LogoutComponent,
        RegisterComponent
    ],
    providers: [
        // MessageService, UserService, AuthGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
