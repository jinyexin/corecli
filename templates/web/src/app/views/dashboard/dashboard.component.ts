import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {BaseAuthenticateComponent} from "../../components/baseAuthenticateComponent";
import {MessageService} from "../../service/messageService";

@Component({
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent extends BaseAuthenticateComponent {

    constructor(protected _router: Router, private _messageService: MessageService) {
        super(_router, _messageService);
    }

}
