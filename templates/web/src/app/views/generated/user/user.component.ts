import {Component, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ModalDirective} from "ngx-bootstrap";
import {Observable, Subject} from "rxjs";

import {catchError, debounceTime, map, switchMap, tap} from 'rxjs/operators';

import {BaseAuthenticateComponent} from "../../../components/baseAuthenticateComponent";
import {PagedData} from "../../../module/pagination/PagedData";
import {PageInfo} from "../../../module/pagination/PageInfo";
import {User} from "../../../module/User";
import {MessageService} from "../../../service/messageService";
import {UserService} from "../../../service/userService";

@Component({
    templateUrl: 'user.component.html'
})
export class UserComponent extends BaseAuthenticateComponent {

    @ViewChild('editModal') public editModal: ModalDirective;
    @ViewChild('deleteModal') public deleteModal: ModalDirective;
    @ViewChild('dtTable') table: any;

    searchSteam = new Subject<PageInfo>();
    searchTerm = "";

    users$: Observable<User[]>;
    userToEdit: User = new User();
    userToDelete: User = new User();

    pageInfo: PageInfo = new PageInfo();

    constructor(protected _router: Router, private _messageService: MessageService, private _userService: UserService) {
        super(_router, _messageService);
    }

    ngOnInit() {
        this.users$ = this.searchSteam.pipe(
            debounceTime(500),
            tap(_ => this._messageService.globalLoading$.next(true)),
            switchMap((search: PageInfo) => this._userService.pagination(search)),
            map((result: PagedData<User>) => {
                this.pageInfo = result.page;
                this.table.bodyComponent.updateOffsetY(this.pageInfo.offset);
                this.table.offset = this.pageInfo.offset;
                this._messageService.globalLoading$.next(false);
                return result.data;
            }),
            catchError(err => {
                this._messageService.globalLoading$.next(false);
                this.handleError(err);
                return [];
            })
        );
    }

    ngAfterViewInit() {
        this.pageInfo.columns = [{name: "username"}];
        this.search();
    }

    onTableChange(page) {
        if (page.offset || page.offset === 0) {
            this.pageInfo.offset = page.offset;
        }
        if (page.sorts) {
            this.pageInfo.sorts = page.sorts.map(old => {
                return {dir: old.dir, column: old.prop}
            });
        }
        this.search(false);
    }

    search(toFirstPage = true) {
        this.pageInfo.searchAllColumn = this.searchTerm;
        if (toFirstPage) {
            this.pageInfo.offset = 0;
        }
        this.searchSteam.next(this.pageInfo);
    }

    editUser(user: User) {
        this.userToEdit = user;
        this.editModal.show();
    }

    createUser() {
        this.userToEdit = new User();
        this.editModal.show();
    }

    deleteUser(user: User) {
        this.userToDelete = user;
        this.deleteModal.show();
    }

    create() {
        this._userService.create(this.userToEdit).subscribe(
            () => {
                this.handleSuccess("create success");
                this.editModal.hide();
                this.search();
            },
            err => this.handleError(err)
        );
    }

    update() {
        this._userService.update(this.userToEdit).subscribe(
            () => {
                this.handleSuccess("update success");
                this.editModal.hide();
                this.search();
            },
            err => this.handleError(err)
        );
    }

    delete() {
        this._userService.delete(this.userToDelete.id).subscribe(
            () => {
                this.handleSuccess("delete success");
                this.deleteModal.hide();
                this.search();
            },
            err => this.handleError(err)
        );
    }

}
