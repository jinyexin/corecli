import {NgModule} from '@angular/core';
import {SharedModules} from "../../../shared/sharedModules";
import {UserRoutingModule} from "./user-routing.module";
import {UserComponent} from "./user.component";


@NgModule({
    imports: [
        SharedModules,
        UserRoutingModule
    ],
    declarations: [UserComponent],
    providers: []// userService is provided in app scope, do not reimport here!!!
})
export class UserModule {
}
