import {Component} from '@angular/core';

// a very simple outlet of all generated components
@Component({
  template: `
    <router-outlet></router-outlet>
  `
})
export class GeneratedComponent {

  constructor() {
  }

}
