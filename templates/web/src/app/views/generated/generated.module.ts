import {NgModule} from '@angular/core';
import {GeneratedRoutingModule} from "./generated-routing.module";
import {GeneratedComponent} from "./generated.component";


@NgModule({
    imports: [
        GeneratedRoutingModule
    ],
    declarations: [GeneratedComponent],
    providers: []
})
export class GeneratedModule {
}
