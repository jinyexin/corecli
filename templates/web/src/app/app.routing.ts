import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from "./components/app-login/login.component";
import {LogoutComponent} from "./components/app-logout/logout.component";
import {RegisterComponent} from "./components/app-register/register.component";

// Import Containers
import {
    FullLayoutComponent
} from './containers/full-layout';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },
    {
        path: '',
        component: FullLayoutComponent,
        data: {
            title: 'Home'
        },
        children: [
            {
                path: 'dashboard',
                loadChildren: './views/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'generated',
                loadChildren: './views/generated/generated.module#GeneratedModule'
            }
        ]
    },
    {path: "login", component: LoginComponent},
    {path: "logout", component: LogoutComponent},
    {path: "register", component: RegisterComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
