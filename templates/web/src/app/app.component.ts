import {Component} from '@angular/core';
import {NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {MessageService} from "./service/messageService";
import {debounceTime} from "rxjs/operators";


@Component({
    // tslint:disable-next-line
    selector: 'body',
    template: `
        <ngx-loading [show]="loading" [config]="{ backdropBorderRadius: '14px' }"></ngx-loading>
        <router-outlet></router-outlet>
    `
})
export class AppComponent {
    public loading = false;

    constructor(public router: Router, protected a_notificationService: ToastrService, private messageService: MessageService) {
        // app level message handle here
        this.messageService.messages$.subscribe(
            appMessage => {
                switch (appMessage.type) {
                    case "error": {
                        this.a_notificationService.error(appMessage.content, appMessage.title);
                        break;
                    }
                    case "info": {
                        this.a_notificationService.info(appMessage.content, appMessage.title);
                        break;
                    }
                    case "success": {
                        this.a_notificationService.success(appMessage.content, appMessage.title);
                        break;
                    }
                    case "warning": {
                        this.a_notificationService.warning(appMessage.content, appMessage.title);
                        break;
                    }
                }
            }
        );
        // app level loading effect
        this.messageService.globalLoading$.pipe(debounceTime(500)).subscribe(
            async (loadStatus: boolean) => {
                this.loading = await loadStatus;
            }
        );
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationStart) {
                this.messageService.globalLoading$.next(true);
            }
            if (event instanceof NavigationEnd ||
                event instanceof NavigationError ||
                event instanceof NavigationCancel) {
                this.messageService.globalLoading$.next(false);
            }
        });
    }
}
