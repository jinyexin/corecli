import {Component} from "@angular/core";
import {FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {User} from "../../module/User";
import {AuthGuard} from "../../service/authGuard";
import {ServiceError} from "../../service/error/ServiceError";
import {MessageService} from "../../service/messageService";
import {UserService} from "../../service/userService";
import {BaseReactiveFormComponent} from "../baseReactiveFormComponent";


@Component({
    selector: "app-login-cmp",
    templateUrl: "./login.component.html",
})
export class LoginComponent extends BaseReactiveFormComponent<User> {

    successURL = "/dashboard";
    registerURL = "/register";

    constructor(protected a_router: Router,
                private _userService: UserService,
                private messageService: MessageService,
                protected _fb: FormBuilder,
                private authGuard: AuthGuard) {
        super(a_router, messageService, _fb);
        this.domainObject = new User();
        super.buildForm();
    }

    getFormErrors() {
        return {"username": "", "password": ""};
    }

    getValidationMessages() {
        return {
            "username": {
                "required": "Username is required.",
                // "minlength": "Username must be at least 4 characters long.",
                "maxlength": "Username cannot be more than 24 characters long.",
                // "forbiddenName": "Someone named \"jack\" cannot be a username."
            },
            "password": {
                "required": "Password is required."
            }
        };
    }

    _buildForm() {
        this.aForm = this.fb.group({
            "username": [this.domainObject.username, [
                Validators.required,
                // Validators.minLength(4),
                Validators.maxLength(24),
                // forbiddenNameValidator(/jack/i)
            ]
            ],
            "password": [this.domainObject.password, Validators.required],
        });
    }

    onSubmit() {
        super.onSubmit();
        this.login();
    }

    login() {
        this._userService.login(this.domainObject.username, this.domainObject.password).subscribe(
            (result) => {
                localStorage.setItem("loginUser", JSON.stringify(result.detail));
                localStorage.setItem("token", result.token);
                this.a_router.navigate([this.authGuard.backURL ? this.authGuard.backURL : this.successURL]);
            },
            (err: ServiceError) => {
                if (err.statusCode === 0) {
                    this.handleError("网络失败,请检查网络.");
                } else {
                    this.handleError("登录失败,请检查用户名及密码.");
                }
            }
        );
    }

    goRegister() {
        this.a_router.navigate([this.registerURL]);
    }

}
