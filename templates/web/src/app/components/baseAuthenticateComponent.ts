/**
 * Created by enixjin on 1/3/17.
 */
import {AfterViewInit, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {User} from "../module/User";
import {ServiceError} from "../service/error/ServiceError";
import {MessageService} from "../service/messageService";


export abstract class BaseAuthenticateComponent implements AfterViewInit, OnInit {
    loginUser: User;
    token: string;

    constructor(protected a_router: Router, private a_messageService: MessageService) {
        try {
            this.loginUser = JSON.parse(localStorage.getItem("loginUser"));
            this.token = localStorage.getItem("token");
        } catch (e) {
            localStorage.clear();
        }
    }

    ngOnInit() {
        // do nothing now
    }

    ngAfterViewInit() {
        // do nothing now
    }

    updateLocalUser() {
        localStorage.setItem("loginUser", JSON.stringify(this.loginUser));
    }

    refreshLocalUser() {
        this.loginUser = JSON.parse(localStorage.getItem("loginUser"));
    }

    jumpToLogin() {
        this.a_router.navigate(["/login"]);
    }

    handleError(error: string | ServiceError) {
        if (typeof error === "string") {
            this.a_messageService.pushMessage({title: "Error", content: error, type: "error"});
        } else {
            this.a_messageService.pushMessage({title: error.message, content: error.error, type: "error"});
        }
    }

    logout() {
        localStorage.clear();
    }

    handleSuccess(message) {
        this.a_messageService.pushMessage({title: "Success", content: message, type: "success"});
    }
}
