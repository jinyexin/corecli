import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {BaseAuthenticateComponent} from "../../components/baseAuthenticateComponent";
import {MessageService} from "../../service/messageService";

@Component({
    template: ''
})
export class LogoutComponent extends BaseAuthenticateComponent {

    constructor(protected _router: Router, private _messageService: MessageService) {
        super(_router, _messageService);
        this.logout();
        this._router.navigate(["/login"]);
    }

}
