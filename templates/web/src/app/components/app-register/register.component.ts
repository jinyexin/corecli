import {Component} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {User} from "../../module/User";
import {AuthGuard} from "../../service/authGuard";
import {MessageService} from "../../service/messageService";
import {UserService} from "../../service/userService";
import {BaseReactiveFormComponent} from "../baseReactiveFormComponent";

@Component({
    selector: "app-register-cmp",
    templateUrl: 'register.component.html'
})
export class RegisterComponent  extends BaseReactiveFormComponent<User>{

    constructor(protected a_router: Router,
                private _userService: UserService,
                private messageService: MessageService,
                protected _fb: FormBuilder,
                private authGuard: AuthGuard) {
        super(a_router, messageService, _fb);
        this.domainObject = new User();
        super.buildForm();
    }

    getFormErrors() {
        return {"username": "", "password": ""};
    }

    getValidationMessages() {
        return {
            "username": {
                "required": "Username is required.",
                // "minlength": "Username must be at least 4 characters long.",
                "maxlength": "Username cannot be more than 24 characters long.",
            },
            "password": {
                "required": "Password is required."
            }
        };
    }

    _buildForm() {
        this.aForm = this.fb.group({
            "username": [this.domainObject.username, [
                Validators.required,
                // Validators.minLength(4),
                Validators.maxLength(24),
            ]
            ],
            "password": [this.domainObject.password, Validators.required],
        });
    }

}
