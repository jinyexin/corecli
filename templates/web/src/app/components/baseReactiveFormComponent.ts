import {FormBuilder, FormGroup} from "@angular/forms";
/**
 * Created by enixjin on 3/10/17.
 */
import {Router} from "@angular/router";
import {BaseModule} from "../module/BaseModule";
import {MessageService} from "../service/messageService";
import {BaseAuthenticateComponent} from "./baseAuthenticateComponent";


export abstract class BaseReactiveFormComponent<T extends BaseModule> extends BaseAuthenticateComponent {
    // form attributes
    active = true;
    aForm: FormGroup;
    domainObject: T;
    formErrors: any = {};
    validationMessages = {};

    constructor(protected a_router: Router, private _messageService: MessageService, protected fb: FormBuilder) {
        super(a_router, _messageService);
        this.formErrors = this.getFormErrors();
        this.validationMessages = this.getValidationMessages();
    }

    abstract _buildForm();

    abstract getFormErrors();

    abstract getValidationMessages();

    buildForm(): void {
        this._buildForm();

        this.aForm.valueChanges
            .subscribe(data => this.onValueChanged(data));

        this.onValueChanged(); // (re)set validation messages now
    }

    onSubmit() {
        this.domainObject = this.aForm.value;
    }

    onValueChanged(data?: any) {
        if (!this.aForm) {
            return;
        }
        const form = this.aForm;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = "";
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + "\r\n";
                }
            }
        }
    }
}
