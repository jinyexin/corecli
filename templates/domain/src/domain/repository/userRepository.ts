import {User} from "../entity/User";
// import * as winston from "winston";
// import * as jwt from "jsonwebtoken";
import {baseUserRepository, Repository} from "@jinyexin/core";

@Repository()
export class userRepository extends baseUserRepository<User> {

    constructor() {
        super(User);
    }

    // miniappLogin(openid: string): Promise<any> {
    //     return this.search({wx_open_id: openid}).then(
    //         result => {
    //             if (result.length > 0) {
    //                 return {id: result[0].id, openid, token: this.generateJWTMiniapp({id: result[0].id, openid})};
    //             } else {
    //                 return this.insert({open_id: openid} as User).then(
    //                     id => {
    //                         return {id, openid, token: this.generateJWTMiniapp({id, openid})}
    //                     }
    //                 );
    //             }
    //         }
    //     );
    // }
    //
    // generateJWTMiniapp(user: any): string {
    //     winston.debug(`generate token for miniapp  user`);
    //     let config = global.config;
    //     return jwt.sign(user, config.jwtSecKey, {expiresIn: config.jwtTimeout});
    // }

}