/**
 * Created by enixjin on 6/13/18.
 */
import {Inject, SendMessage, SubscribeMessage, WebSocketGateway, logger} from "@jinyexin/core";
import {Server} from "http";
import * as SocketIO from "socket.io";
import {userRepository} from "../domain/repository/userRepository";

@WebSocketGateway()
export class Gateway {

    @Inject()
    userRepository: userRepository;

    @SendMessage({interval: 5000, event: "device data"})
    async deviceData() {
        return await this.userRepository.listAll();
    }

    @SubscribeMessage({event: "test"})
    async test(io: SocketIO.Server, socket: SocketIO.Socket, data: any) {
        logger.warn("get test message:", JSON.stringify(data));
    }

}

export interface Gateway {
    io: SocketIO.Server;
    init(server: Server)
}