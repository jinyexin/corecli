/**
 * Created by enixjin on 12/13/16.
 */
import {
    CRUDController,
    defaultException,
    encryption,
    ICRUDController,
    Inject,
    logger,
    Post,
    serviceException,
    serviceFactoryImpl
} from "@jinyexin/core";
// import {validateRoleEngineImpl} from "../service/validateRoleEngine/impl/validateRoleEngineImpl";
import {User} from "../domain/entity/User";
import {userRepository} from "../domain/repository/userRepository";

@CRUDController(User)
export class userController {

    @Inject()
    repository: userRepository;

    @Post({url: '/register', auth: false})
    register(req, res, next) {
        logger.info("creating user:" + JSON.stringify(req.body));
        req.body.create_date = new Date();
        if (req.body.password) {
            req.body.password = encryption.encryptMd5(req.body.password);
        }
        this.repository.insert(req.body)
            .then((id) => this.repository.queryByID(id))
            .then(
                (insertUser) => res.status(200).jsonp({
                    id: insertUser.id,
                    token: this.repository.generateJWT(insertUser),
                    detail: insertUser
                }),
            );
    }

    @Post({url: '/login', auth: false})
    async login(req, res, next): Promise<any> {
        if (req.body && req.body.username && req.body.password) {
            logger.info("login user:" + req.body.username);
            let result = await serviceFactoryImpl.getInstance().getAuthenticationService().login(req.body.username, req.body.password, this.repository)
            if (result) {
                return result;
            } else {
                throw defaultException.WARNING_authFail; //build in exception
            }
        } else {
            throw new serviceException("user or password missing", 400); //customize exception
        }
    }

}

export interface userController extends ICRUDController<User> {
}
