/**
 * Created by enixjin on 2/12/18.
 */
import {FileUploadController, IFileController} from "@jinyexin/core";


@FileUploadController({
    baseUrl: "/image",
    uploadFolder: global.config.imagePath,
    nameSingle: "image",
    nameMulti: "images"
})
export class imageController {

}

export interface imageController extends IFileController {
}
