/**
 * Created by enixjin on 7/18/16.
 */
global.config = require('../config.js');

import {App} from "@jinyexin/core";
import {WechatServerController} from "./controller/wechatServerController";

// uncomment following code to enable socket.io
// import {Gateway} from "./websocket/gateway";

let application = new App(
    {
        appName: "wechat",
        endpoint: "",
        controllers: [
            WechatServerController
        ],
        // uncomment following code to enable socket.io
        // gateway: Gateway,
        initialDatabase: false,
        enableSwagger: false
    }
);

application.init();
