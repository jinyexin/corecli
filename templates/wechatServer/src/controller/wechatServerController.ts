import {IController} from "@jinyexin/core";
import {abstractHandler, WechatController} from "@jinyexin/wechat";
import {WechatServerHandler} from "../handler/wechatServerHandler";

/**
 * Created by enixjin on 3/29/18.
 */

export class WechatServerController extends WechatController {

    handler: abstractHandler = new WechatServerHandler();

}

export interface WechatServerController extends IController {

}