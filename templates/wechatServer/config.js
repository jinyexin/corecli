/**
 * Created by enixjin on 9/24/15.
 */
var config = {};

config.servicePort = 5200;
config.httpJsonPort = 5000;
config.socketJsonPort = 5300;



//wechat config
config.wechat = {};
config.wechat.token = "enixjin";
config.wechat.tokenTimeout = (90 * 60) * 1000;//refresh token every 90 mins
config.wechat.appID = "123456789";
config.wechat.appsecret = "123456789";
config.wechat.mch_id = "10000100";
config.wechat.key = "123456789";
config.wechat.notify_url = "http://wechat.enixjin.net/pay";

//domain config
config.domainServer = "domain.yoursite.com";
config.domainPort = 9802;
config.domainWechatLoginUrl = "/loginWechat";
config.domainOrderAPI = "/api/purchaseOrders/order";

config.logLevel = 'debug';
config.logFile = '/sandbox/log/wechat/log.txt';
config.logLevelFile = "debug";

config.jwtSecKey = 'enixjin';
config.jwtTimeout = '90d';

config.imagePath = "/sandbox/temp";

module.exports = config;
