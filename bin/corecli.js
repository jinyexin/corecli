#! /usr/bin/env node
const inquirer = require('inquirer');
inquirer.registerPrompt('fuzzypath', require('inquirer-fuzzy-path'));
// const prompt = inquirer.createPromptModule();
const ora = require('ora');
const fs = require('fs');
let config = {
    centralizedDB: {
        connectionLimit: 5,
        host: 'localhost',
    },
    logLevel: 'info',
    logFile: 'log.txt'
};

ora(`A code generator for @jinyexin/core.(cli version:${require("../package.json").version})`).info();

inquirer
    .prompt([{
        type: "fuzzypath",
        name: "outpath",
        pathFilter: (isDirectory, nodePath) => isDirectory,
        message: `where should be code generated?`,
        default: process.cwd(),
        suggestOnly: false,
        rootPath: process.cwd()
    }])
    .then(
        answer => {
            if (fs.existsSync(answer.outpath)) {
                global._outpath = answer.outpath;
                return "";
            } else {
                process.exit();
            }
        }
    )
    .then(() => {
        inquirer.prompt([{
            type: "input",
            name: "confirmDelete",
            message: `are you sure to delete everything in "${global._outpath}"?(y/N)`,
            default: "N",
            validate: function (value) {
                if (value !== "n" && value !== "N") {
                    return true;
                }
                process.exit();
            }
        }, {
            type: "input",
            name: "host",
            message: `database host?`,
            default: "localhost",
            validate: function (value) {
                if (value) {
                    return true;
                }
                return "please type your database host";
            }
        }, {
            type: "input",
            name: "dbuser",
            message: `database username?`,
            default: "root",
            validate: function (value) {
                if (value) {
                    return true;
                }
                return "please type your database username";
            }
        }, {
            type: "password",
            name: "dbpassword",
            message: `database password?`,
            default: "root",
            validate: function (value) {
                if (value) {
                    return true;
                }
                return "please type your database password";
            }
        }, {
            type: "input",
            name: "database",
            message: `database name?`,
            validate: function (value) {
                if (value) {
                    return true;
                }
                return "please type your database name";
            }
        }])
            .then(answer => {
                config.centralizedDB.host = answer.host;
                config.centralizedDB.database = answer.database;
                config.centralizedDB.user = answer.dbuser;
                config.centralizedDB.password = answer.dbpassword;
                global._database = answer.database;
                global.table_prefix = answer.database;

                global._dirname = __dirname;
            })
            .then(() => {
                inquirer.prompt([{
                    type: "input",
                    name: "prefix",
                    message: `table name prefix?`,
                    default: global.table_prefix,
                    validate: function (value) {
                        if (value) {
                            return true;
                        }
                        return "please type your database table name prefix";
                    }
                }]).then(
                    ans => {
                        global.config = config;
                        global.table_prefix = ans.prefix;
                        require("../dist/generator.min");
                    }
                )
            });
    }).catch(err => {
    ora(`error: ${err}`).fail();
    process.exit(0);
});