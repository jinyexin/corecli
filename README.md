# core cli

A cli tools for generate projects from database

@see [core](https://www.npmjs.com/package/@jinyexin/core) for more information

[source code](https://gitlab.com/jinyexin/corecli)
    
## changelog

see [CHANGELOG](https://gitlab.com/jinyexin/corecli/blob/master/CHANGELOG.md) 

## Generate from database

    create ${database} in MySQL, all table should have ${perfix}, e.g tb_user,tb_course

    npm i @jinyexin/corecli -g
    
    create project folder
    
    in project folder
    
    corecli

## Run project

    cd generated/code/folder
    
    //domain
    cd domain
    
    git init
    
    npm i && npm start
    
    //web
    cd web
    
    npm i && npm start
    
    visit localhost:3000/api-docs for api doc
    
    visit localhost:4200 for web

## Debug project

* for vscode, see .vscode/launch.json

* for webstorm add a nodejs configuration with "Node parameters"="-r ts-node/register" and "Javascript file"="src/app.ts"
