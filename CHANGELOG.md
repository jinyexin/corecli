## @jinyexin/corecli changelog

    3.3.8 fix wechat server template
    3.3.3 fix web service bug and update swagger
    3.3.1 update app config to allow enable/disable database and swagger
    3.3.0 move app initial code to core for reduce verbose code
    3.2.5 fix "npm run build:prod" fail with pin terser version for now
    3.2.2 add tslint and husky to domain
    3.2.0 align version with @jinyexin/core 3.2.0
    3.1.2 support debug domain in vscode and webstorm
    3.1.1 upgrade dependencies
    3.1.0 use terser-webpack-plugin instead of UglifyJsPlugin for es6+ support
    3.0.16 change TypeScript compiler target to es2017
    3.0.15 fix generated swagger yaml format
    3.0.14 put core and corecli in seperate repository
    3.0.13 refactor code structure
    3.0.12 upgrade angular to 7
    3.0.11 use inuqirer for corecli
    3.0.9 add docker to domain
    3.0.8 improve http middleware(callbacks)
    3.0.7 improve /${entity}/pagination, see generated swagger api doc for more information
    3.0.6 upgrade to typescript 3
    3.0.5 upgrade webpack to latest and improve swagger api doc
    3.0.3 expose a logger instead of direct usage of winston
    3.0.0 upgrade api-doc to swagger, visit http://localhost:3000/api-doc for api doc with swagger. (use https://editor.swagger.io to edit /domain/swagger.yaml as will)
    2.2.17 update to webpack4
    2.2.16 update to winston3 and initial support for swagger
    2.2.12 update core log to winston.silly and fix debug info is written in error log while working with pm2
    2.2.6 merge default CRUD requests e.g. {Post} /api/users {...} for insert, {Post} /api/users [{...},{...}] for batch insert
    2.2.0 initial support for socket.io
    2.1.4 remove rxjs-compat
    2.1.2 update service in web for new DI of angular6
    2.1.0 upgrade to angular6 and align major version with core
    0.1.5 support only generate/regenerate for one table in current project @see -t
