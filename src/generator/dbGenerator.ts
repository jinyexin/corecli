/**
 * Created by enixjin on 16-12-30.
 */


// global.config = require("../config");
let table_prefix: string = global.table_prefix;

import {centralizedMySQL, initDB, logger} from "@jinyexin/core";
import * as ejs from "ejs";
import * as fs from "fs-extra";

initDB("MySQL");
let total = 100;
let count = 0;
let _outpath = global._outpath;
let domain_source_path = _outpath + "/domain/src/";
let web_source_path = _outpath + "/web/src/app/";
let controllers = [];

let tableDefinitions: tableDefinition[] = [];

const logSymbols = require('log-symbols');
const ora = require('ora');
const spinner = ora("Initializ...").start();
const _table = global._table;

//main
centralizedMySQL.getInstance().query("show tables", null).then(
    (tables) => {
        let config = global.config;
        if (!_table) {
            total = tables.length * 3; // 3 task for each table

            fs.emptyDirSync(_outpath);
            spinner.stopAndPersist({symbol: logSymbols.success, text: `clearing output folder ${_outpath}`});

            fs.copySync(global._dirname + "/../templates/domain", _outpath + "/domain");
            spinner.stopAndPersist({symbol: logSymbols.success, text: `copying domain template..`});

            fs.copySync(global._dirname + "/../templates/web", _outpath + "/web");
            spinner.stopAndPersist({symbol: logSymbols.success, text: `copying web template..`});

            fs.copySync(global._dirname + "/../templates/wechatServer", _outpath + "/wechatServer");
            spinner.stopAndPersist({symbol: logSymbols.success, text: `copying wechat server template..`});

            spinner.stopAndPersist({symbol: logSymbols.info, text: `found ${tables.length} tables...`});

            initialDirs();

            tables.forEach(
                (table, index) => {
                    let tablename = table["Tables_in_" + config.centralizedDB.database];
                    centralizedMySQL.getInstance().query(
                        "show columns in " + tablename, null
                    ).then(
                        (columns) => {
                            spinner.stopAndPersist({
                                symbol: logSymbols.success,
                                text: `generating for table ${tablename}, ${index + 1}/${tables.length}`
                            });
                            if (!tablename.startsWith(table_prefix)) {
                                spinner.fail(`table name perfix is not matching [${table_prefix}]!!!`);
                                fs.emptyDirSync(_outpath);
                                process.exit(0);
                            }
                            generateEntity(tablename, columns);
                            generateRepository(tablename);
                            generateController(tablename);
                        }
                    )
                }
            );
        } else {
            spinner.stopAndPersist({
                symbol: logSymbols.warning,
                text: `partial model, only generate code for table [${_table}]`
            });

            total = 3;

            tables.forEach(
                (table, index) => {
                    let tablename = table["Tables_in_" + config.centralizedDB.database];
                    if (tablename === _table) {
                        centralizedMySQL.getInstance().query(
                            "show columns in " + tablename, null
                        ).then(
                            (columns) => {
                                spinner.stopAndPersist({
                                    symbol: logSymbols.success,
                                    text: `generating for table ${tablename}`
                                });
                                generateEntity(tablename, columns);
                                generateRepository(tablename);
                                generateController(tablename);
                            }
                        )
                    }
                }
            );
        }
    }
);

function initialDirs() {
    // domain
    fs.ensureDirSync(domain_source_path + "controller");
    fs.ensureDirSync(domain_source_path + "domain");
    fs.ensureDirSync(domain_source_path + "domain/repository");
    fs.ensureDirSync(domain_source_path + "domain/entity");
    // web
    spinner.stopAndPersist({symbol: logSymbols.success, text: `initial output dirs...`});
}

function generateController(tableName: string) {
    let entityName = getEntityName(tableName);
    let lowerEntityName = getLowerFirst(entityName);
    if (entityName === "User") {
        count++;
        if (count == total) {
            finalize();
        } else {
            return;
        }
    }
    controllers.push(`${lowerEntityName}Controller`);
    let fileName = lowerEntityName + "Controller.ts";
    fs.ensureFileSync(domain_source_path + "controller/" + fileName);
    ejs.renderFile(
        global._dirname + "/../ejs/domain/controller.ejs",
        {
            tableName,
            entityName,
            lowerEntityName,
        },
        (err, str) => {
            fs.writeFileSync(
                domain_source_path + "controller/" + fileName,
                str
            );
            count++;
            if (count == total) {
                finalize();
            }
        });
}

function generateRepository(tableName: string) {
    let entityName = getEntityName(tableName);
    if (entityName === "User") {
        count++;
        if (count == total) {
            finalize();
        } else {
            return;
        }
    }
    let lowerEntityName = getLowerFirst(entityName);
    let fileName = lowerEntityName + "Repository.ts";

    fs.ensureFileSync(domain_source_path + "domain/repository/" + fileName);
    ejs.renderFile(
        global._dirname + "/../ejs/domain/repository.ejs",
        {
            tableName,
            entityName,
            lowerEntityName,
        },
        (err, str) => {
            fs.writeFileSync(
                domain_source_path + "domain/repository/" + fileName,
                str
            );
            count++;
            if (count == total) {
                finalize();
            }
        });
}

function generateEntity(tableName: string, columns: any[]) {
    let entityName = getEntityName(tableName);
    let fileName = entityName + ".ts";
    let userImport = "";
    let baseDomainObject = "baseDomainObject";
    //User with login
    if (entityName === "User") {
        userImport = ", baseUser";
        baseDomainObject = "baseUser";
        columns = columns.filter(c => c.Field !== "password");
    }
    let lowerEntityName = getLowerFirst(entityName);
    tableDefinitions.push({
        tableName,
        entityName,
        lowerEntityName,
        columns: columns.filter(filterBaseDomain).map(_ => {
            return {field: _.Field, type: getTypeFromDB(_.Type), swaggerType: getTypeForSwagger(_.Type)};
        })
    });
    let columnDefinition = columns
        .filter(filterBaseDomain)
        .map(_ => `    ${_.Field}: ${getTypeFromDB(_.Type)};`)
        .join("\r\n");
    fs.ensureFileSync(domain_source_path + "domain/entity/" + fileName);
    ejs.renderFile(
        global._dirname + "/../ejs/domain/entity.ejs",
        {
            tableName,
            userImport,
            baseDomainObject,
            entityName,
            lowerEntityName,
            columnDefinition
        },
        (err, str) => {
            fs.writeFileSync(
                domain_source_path + "domain/entity/" + fileName,
                str
            );
            count++;
            if (count == total) {
                finalize();
            }
        });
}

function generateApp() {
    fs.ensureFileSync(domain_source_path + "app.ts");
    ejs.renderFile(
        global._dirname + "/../ejs/domain/app.ejs",
        {
            imports: controllers.map(_ => `import {${_}} from "./controller/${_}";`).join("\r\n"),
            controllers: controllers.map(_ => `${_},`).join("\r\n            ")
        },
        (err, str) => {
            fs.writeFileSync(
                domain_source_path + "app.ts",
                str
            );
            spinner.stopAndPersist({symbol: logSymbols.success, text: `generating server.ts for controllers...`});
        });

    return;
}

function generateSwagger() {
    fs.ensureFileSync(_outpath + "/domain/swagger.yaml");
    ejs.renderFile(
        global._dirname + "/../ejs/domain/swagger.ejs",
        {
            tableDefinitions
        },
        (err, str) => {
            fs.writeFileSync(
                _outpath + "/domain/swagger.yaml",
                str
            );
            spinner.stopAndPersist({symbol: logSymbols.success, text: `create swagger api doc...`});
        });

    return;
}

function generateDomainConfig() {
    fs.ensureFileSync(_outpath + "/domain/config.js");
    ejs.renderFile(
        global._dirname + "/../ejs/domain/config.ejs",
        {
            db_server: global.config.centralizedDB.host,
            db_user: global.config.centralizedDB.user,
            db_password: global.config.centralizedDB.password,
            database: global.config.centralizedDB.database
        },
        (err, str) => {
            fs.writeFileSync(
                _outpath + "/domain/config.js",
                str
            );
            spinner.stopAndPersist({symbol: logSymbols.success, text: `generating config.js for domain...`});
        });

    return;
}

function finalize() {
    if (!_table) {
        generateSwagger();
        generateDomainConfig();
        generateApp();
        generateWebNav();
        generateWebRouting();
        generateWebPages();
        spinner.stopAndPersist({symbol: logSymbols.success, text: `ALL task done...`});
        ora(`to start domain, go to generated/domain, and run "npm i && npm start"`).info();
        ora(`to start web, go to generated/web, and run "npm i && npm start"`).info();
        process.exit();
    } else {
        partialWebPages();
        spinner.stopAndPersist({symbol: logSymbols.success, text: `Generate for table [${_table}] is done...`});
        ora(`IF 1ST TIME FOR NEW TABLE, DO NOT FORGET TO UPDATE FOLLOWING TO CODE TO WORK!!!`).warn();
        ora(`1. update domain/src/server.ts to register controller`).info();
        ora(`2. update web/src/app/_nav.ts for left nav bar`).info();
        ora(`3. update web/src/app/views/generated/generated-routing.module.ts for routing`).info();
        ora(`4. update web/src/app/constants.ts for domain api`).info();
        process.exit();
    }

}

function generateWebNav() {
    fs.ensureFileSync(web_source_path + "_nav.ts");
    ejs.renderFile(
        global._dirname + "/../ejs/web/_nav.ejs",
        {
            tableDefinitions
        },
        (err, str) => {
            fs.writeFileSync(
                web_source_path + "_nav.ts",
                str
            );
            spinner.stopAndPersist({symbol: logSymbols.success, text: `create nav for web...`});
        });

    return;
}

function generateWebRouting() {
    fs.ensureFileSync(web_source_path + "views/generated/generated-routing.module.ts");
    ejs.renderFile(
        global._dirname + "/../ejs/web/generated-routing.module.ejs",
        {
            tableDefinitions
        },
        (err, str) => {
            fs.writeFileSync(
                web_source_path + "views/generated/generated-routing.module.ts",
                str
            );
            spinner.stopAndPersist({symbol: logSymbols.success, text: `create routing for web...`});
        });

    return;
}

function partialWebPages() {

    tableDefinitions.filter(td => td.lowerEntityName !== "user").map(td => {
        // entity
        ejs.renderFile(
            global._dirname + "/../ejs/web/entity.ejs",
            {
                tableDefinition: td
            },
            (err, str) => {
                fs.writeFileSync(
                    web_source_path + "module/" + td.entityName + ".ts",
                    str
                );
            });
        // service
        ejs.renderFile(
            global._dirname + "/../ejs/web/service.ejs",
            {
                tableDefinition: td
            },
            (err, str) => {
                fs.writeFileSync(
                    web_source_path + "service/" + td.lowerEntityName + "Service.ts",
                    str
                );
            });
        // page
        fs.ensureDirSync(web_source_path + "/views/generated/" + td.lowerEntityName);
        ejs.renderFile(
            global._dirname + "/../ejs/web/entity.component.html.ejs",
            {
                tableDefinition: td
            },
            (err, str) => {
                fs.writeFileSync(
                    web_source_path + "/views/generated/" + td.lowerEntityName + "/" + td.lowerEntityName + ".component.html",
                    str
                );
            });
        ejs.renderFile(
            global._dirname + "/../ejs/web/entity.module.ts.ejs",
            {
                tableDefinition: td
            },
            (err, str) => {
                fs.writeFileSync(
                    web_source_path + "/views/generated/" + td.lowerEntityName + "/" + td.lowerEntityName + ".module.ts",
                    str
                );
            });
        ejs.renderFile(
            global._dirname + "/../ejs/web/entity-routing.module.ts.ejs",
            {
                tableDefinition: td
            },
            (err, str) => {
                fs.writeFileSync(
                    web_source_path + "/views/generated/" + td.lowerEntityName + "/" + td.lowerEntityName + "-routing.module.ts",
                    str
                );
            });
        ejs.renderFile(
            global._dirname + "/../ejs/web/entity.component.ts.ejs",
            {
                tableDefinition: td
            },
            (err, str) => {
                fs.writeFileSync(
                    web_source_path + "/views/generated/" + td.lowerEntityName + "/" + td.lowerEntityName + ".component.ts",
                    str
                );
            });
    });
    spinner.stopAndPersist({symbol: logSymbols.success, text: `create partial entitys,service and pages for web...`});
    return;
}

function generateWebPages() {
    // constants
    ejs.renderFile(
        global._dirname + "/../ejs/web/constants.ejs",
        {
            // user api is fix
            tableDefinitions: tableDefinitions.filter(td => td.lowerEntityName !== "user")
        },
        (err, str) => {
            fs.writeFileSync(
                web_source_path + "constants.ts",
                str
            );
        });

    // routing
    ejs.renderFile(
        global._dirname + "/../ejs/web/generated-routing.module.ejs",
        {
            tableDefinitions
        },
        (err, str) => {
            fs.writeFileSync(
                web_source_path + "views/generated/generated-routing.module.ts",
                str
            );
        });

    tableDefinitions.filter(td => td.lowerEntityName !== "user").map(td => {
        // entity
        ejs.renderFile(
            global._dirname + "/../ejs/web/entity.ejs",
            {
                tableDefinition: td
            },
            (err, str) => {
                fs.writeFileSync(
                    web_source_path + "module/" + td.entityName + ".ts",
                    str
                );
            });
        // service
        ejs.renderFile(
            global._dirname + "/../ejs/web/service.ejs",
            {
                tableDefinition: td
            },
            (err, str) => {
                fs.writeFileSync(
                    web_source_path + "service/" + td.lowerEntityName + "Service.ts",
                    str
                );
            });
        // page
        fs.ensureDirSync(web_source_path + "/views/generated/" + td.lowerEntityName);
        ejs.renderFile(
            global._dirname + "/../ejs/web/entity.component.html.ejs",
            {
                tableDefinition: td
            },
            (err, str) => {
                fs.writeFileSync(
                    web_source_path + "/views/generated/" + td.lowerEntityName + "/" + td.lowerEntityName + ".component.html",
                    str
                );
            });
        ejs.renderFile(
            global._dirname + "/../ejs/web/entity.module.ts.ejs",
            {
                tableDefinition: td
            },
            (err, str) => {
                fs.writeFileSync(
                    web_source_path + "/views/generated/" + td.lowerEntityName + "/" + td.lowerEntityName + ".module.ts",
                    str
                );
            });
        ejs.renderFile(
            global._dirname + "/../ejs/web/entity-routing.module.ts.ejs",
            {
                tableDefinition: td
            },
            (err, str) => {
                fs.writeFileSync(
                    web_source_path + "/views/generated/" + td.lowerEntityName + "/" + td.lowerEntityName + "-routing.module.ts",
                    str
                );
            });
        ejs.renderFile(
            global._dirname + "/../ejs/web/entity.component.ts.ejs",
            {
                tableDefinition: td
            },
            (err, str) => {
                fs.writeFileSync(
                    web_source_path + "/views/generated/" + td.lowerEntityName + "/" + td.lowerEntityName + ".component.ts",
                    str
                );
            });
    });
    spinner.stopAndPersist({symbol: logSymbols.success, text: `create entitys,service and pages for web...`});
    return;
}

//utils
function getDocColumns(columns: any[]): string {
    let result = "";
    columns.filter(filterBaseDomain).forEach(
        column => {
            result += ` * @apiParam {${getTypeFromDB(column.Type)}} [${column.Field}] ${column.Field}.
`;
        });
    return result;
}

function getLowerFirst(str: string): string {
    return str.substring(0, 1).toLowerCase() + str.substring(1);
}

function getEntityName(tableName, removePerfix = true) {
    if (table_prefix && removePerfix) {
        tableName = tableName.substring(table_prefix.length);
    }
    let result = tableName.substring(0, tableName.indexOf("_")) + tableName.substring(tableName.indexOf("_") + 1, tableName.indexOf("_") + 2).toUpperCase() + tableName.substring(tableName.indexOf("_") + 2);
    if (result.indexOf("_") > 0) {
        return getEntityName(result, false);
    } else {
        return result.substring(0, 1).toUpperCase() + result.substring(1);
    }
}

function filterBaseDomain(column): boolean {
    return !(column.Field === "id" || column.Field === "create_date" || column.Field === "update_date" || column.Field === "creator" || column.Field === "updater");
}

function getTypeFromDB(dbType: string): string {
    if (dbType.indexOf("int") >= 0 || dbType.indexOf("float") >= 0) {
        return "number";
    } else if (dbType.indexOf("time") >= 0) {
        return "Date";
    } else {
        return "string";
    }
}

function getTypeForSwagger(dbType: string): string {
    if (dbType.indexOf("int") >= 0 || dbType.indexOf("float") >= 0) {
        return "integer";
    } else if (dbType.indexOf("time") >= 0) {
        return `string
              format: date`;
    } else {
        return `string`;
    }
}

class tableDefinition {
    tableName: string;
    entityName: string;
    lowerEntityName: string;
    columns: Array<{ field, type, swaggerType }>;
}
